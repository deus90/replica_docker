```
sudo docker exec -it replica.mysql.1 mysql -uroot -p123
 
CREATE USER 'replicator'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replicator'@'%';
FLUSH PRIVILEGES;

----------> ON SECOND show master status AND GET POSITION FROM master_log_pos AND master_log_file

STOP SLAVE;
CHANGE MASTER TO master_host='192.168.0.22', master_port=3322, master_user='replicator', master_password='123', master_log_file='mysql-bin.000003', master_log_pos=774;
START SLAVE;
```

```
sudo docker exec -it replica.mysql.2 mysql -uroot -p123

CREATE USER 'replicator'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replicator'@'%';
FLUSH PRIVILEGES;

----------> ON FIRST show master status AND GET POSITION FROM master_log_pos AND master_log_file

STOP SLAVE;
CHANGE MASTER TO master_host='192.168.0.11', master_port=3311, master_user='replicator', master_password='123', master_log_file='mysql-bin.000003', master_log_pos=759;
START SLAVE;
```

```
mkdir /var/log/mysql
chmod mysql:mysql /var/log/mysql -R
```